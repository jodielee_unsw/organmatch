import json
from collections import defaultdict
from typing import Optional, Dict, Union
from numpy import random
from random import choices
import names # package that randomly generates names
from pprint import pprint
from uuid import uuid4, UUID
import datetime as dt

class MemberInfo(object): # Mandatory fields

    def __init__(self, user_input: Optional[Dict]):
        if not user_input:
            self.generate_random_member()

        else:
            self.first_name: str = user_input['first_name']
            self.last_name: str = user_input['last_name']
            self.gender: str = user_input['gender']
            self.age: int = user_input['age']
            self.height_cm: float = user_input['height']
            self.weight_kg: float = user_input['weight']
            self.size = self.height_cm * self.weight_kg
            self.blood_type: str = user_input['blood_type']
            self.location = user_input['location'].upper()

    def generate_random_member(self) -> None:
        blood_types = ['O-', 'O+', 'B-', 'B+', 'A-', 'A+', 'AB-', 'AB+']
        blood_types_weights = [0.09, 0.4, 0.02, 0.08, 0.07, 0.31, 0.01, 0.02]
        
        locations_list = ['sydney', 'melbourne', 'adelaide', 'perth']

        # https://www.abs.gov.au/ausstats/abs@.nsf/Lookup/4338.0main+features212011-13
        self.gender = random.choice(['M', 'F'])
        if self.gender == 'M': 
            gender = 'male'
            average_height = 175.6
            average_weight = 85.9
        else: 
            gender = 'female'
            average_height = 161.8
            average_weight = 71.1

        self.first_name = names.get_first_name(gender=gender)
        self.last_name = names.get_last_name()

        self.age = random.normal(loc=55, scale=15, size=1)[0]
        self.height_cm = random.normal(loc=average_height, scale=6.7, size=1)[0]
        self.weight_kg = random.normal(loc=average_weight, scale=8, size=1)[0]
        self.size = self.height_cm * self.weight_kg
        self.blood_type = choices(blood_types, blood_types_weights, k=1)[0]
        self.location = random.choice(locations_list).upper()

class DonorInfo(object):
    def __init__(self, user_input: Optional[Dict]):
        if not user_input: 
            user_input = {}
        d = defaultdict(lambda: None, user_input)
        self.deceased: bool = d['deceased'] or False


class RecipientInfo(object):
    def __init__(self, user_input: Optional[Dict], curr_day: Optional[dt.date] = None):
        if not user_input: 
            user_input = {}

        d = defaultdict(lambda: None, user_input)
        self.estimated_end_date: dt.date = d['estimated_end_date'] or self.random_date(curr_day or dt.date.today(), dt.date(2021,12,31))
        self.day_entered: dt.date = d['day_entered'] or curr_day or dt.date.today()

    def random_date(self, start: dt.date, end: dt.date):
        days = (end - start).days
        r = random.randint(0,days)
        return start + dt.timedelta(days=r)

class Donor(object):
    def __init__(self, user_input: Optional[Dict] = None): # give no input to generate a random donor
        self.member_info = MemberInfo(user_input)
        self.donor_info = DonorInfo(user_input)
    
    def print(self):
        pprint(vars(self.member_info))
        pprint(vars(self.donor_info))

class Recipient(object):
    def __init__(self, user_input: Optional[Dict] = None, curr_day: Optional[dt.date] = None): # give no input to generate a random recipient
        self.member_info = MemberInfo(user_input)
        self.recipient_info = RecipientInfo(user_input, curr_day)

    def print(self):
        pprint(vars(self.member_info))
        pprint(vars(self.recipient_info))

class Pair(object):
    def __init__(self, donor: Donor, recipient: Optional[Recipient] = None):
        self.ID: UUID = uuid4()
        # ['unmatched', 'waiting for approval', 'matched and approved', 'Success']

        # no recipient declared means altruristic donor
        self.ORGAN_TYPE = 'KIDNEY'
        self.STATUS = 'unmatched'
        self.DO_NOT_MATCH = []

        self.donor: Donor = donor
        self.recipient: Recipient = recipient

def object_decoder(obj):
    if '__type__' in obj and obj['__type__'] == 'Donor':
        return Donor(user_input = obj)
    elif '__type__' in obj and obj['__type__'] == 'Recipient':
        return Recipient(user_input = obj)
    else:
        print("Please specify type as Donor or Recipient")


sample_donor = '{"__type__": "Donor", "first_name": "Dave", "last_name": "Smith", "gender": "M", "age": 50, "height": 170, "weight": 83, "blood_type": "A-", "deceased": false, "location": "Sydney"}'
sample_recipient = '{"__type__": "Recipient", "first_name": "Jane", "last_name": "Smith", "gender": "F", "age": 48, "height": 160, "weight": 72, "blood_type": "A+", "time_to_death": 365, "location": "Sydney"}'

donor1 = json.loads(sample_donor, object_hook=object_decoder)
recipient1 = json.loads(sample_recipient, object_hook=object_decoder)

random_donor = Donor()
random_recipient = Recipient()

# new_pair = Pair(donor1, recipient1)
# random_pair = Pair(random_donor, random_recipient)