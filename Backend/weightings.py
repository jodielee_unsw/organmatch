from members import Recipient, Donor, RecipientInfo, Pair
import math
from numpy import mean
from geopy.distance import geodesic
import datetime as dt
from timeit import default_timer as timer

MAX_TIME = 365 # maximum waiting time in days
DAYS_IN_YEAR = 365
KIDNEY_HOURS = 48 # deceased kidney survives for 48 hours

# https://simplemaps.com/data/au-cities we can download the csv of lat/longs from here for all major cities
SYDNEY = [-33.8650, 151.2094]
MELBOURNE = [-37.8136, 144.9631]
ADELAIDE = [-34.9289, 138.6011]
PERTH = [-31.9522, 115.8589]

# r types for testing/simulation
BLOOD = 1000
LOCATION = 2000

TOO_LOW = 4000

def calculate_pair_weight(pair1: Pair, pair2: Pair) -> float:
    # Calculates the weight of the directed edge from pair 1 to pair 2
    return calculate_weight(pair1.donor, pair2.recipient)

def calculate_weight(donor: Donor, recipient: Recipient) -> float:
    ''' 
    Calculates the compatibility of the Donor to Recipient. 
    Returns a float between 0 and 1 with 1 being 100% percent chance of success and 0 being 0% chance.
    '''

    weight = 1
    if not check_blood_compatibility(donor.member_info.blood_type, recipient.member_info.blood_type):
        # print(f"Blood type not compatible: {donor.member_info.blood_type} and {recipient.member_info.blood_type}")
        return BLOOD

    if donor.donor_info.deceased and not check_location(donor.member_info.location, recipient.member_info.location, 'KIDNEY'):
        # print(f'Organ is deceased and location too far: {donor.member_info.location} and {recipient.member_info.location}')
        return LOCATION

    weight *= size_weight(donor.member_info.size, recipient.member_info.size)

    weight *= age_weight(donor.member_info.age, recipient.member_info.age)

    if weight < 0.6:
        # print('Calculated Weight = ', weight, ' is less than 0.6')
        return TOO_LOW

    # print(weight)
    return weight

def check_blood_compatibility(donor_blood_type: str, recipient_blood_type: str):
    donor = blood_antigen_list(donor_blood_type)
    recipient = blood_antigen_list(recipient_blood_type)

    if set(donor).issubset(recipient):
        return True
    else:
        return False

def blood_antigen_list(blood_type: str):
    antigens = []
    if 'A' in blood_type:
        antigens.append('A')
    if 'B' in blood_type:
        antigens.append('B')
    if '+' in blood_type:
        antigens.append('RhD')

    return antigens

def check_location(donor_location: str, recipient_location: str, organ_type: str) -> bool:
    donor_loc = globals()[donor_location]
    recip_loc = globals()[recipient_location]

    distance = geodesic(donor_loc, recip_loc).kilometers
    print(distance)
    # rough estimate 500km = 1 hour
    time_hours = distance/500
    print(time_hours)
    
    organ_time = organ_type + '_HOURS'
    if time_hours < globals()[organ_time]:
        return True
    
    return False

def size_weight(donor_size: float, recipient_size: float) -> float:
    exponent = -5 * (donor_size - recipient_size)**2 / 6969**2
    return math.exp(exponent)

def age_weight(donor_age: float, recipient_age: float) -> float:
    return math.exp(-9/2*((donor_age-recipient_age)/(90-recipient_age))**2)

def boosted_weight(weight, r_info: RecipientInfo, curr_day: dt.date = dt.date.today()):
    # calculating the boost when cycle finding
    return weight * calculate_boost(r_info, curr_day)

def calculate_boost(r_info: RecipientInfo, curr_day: dt.date):
    days_til_death = (r_info.estimated_end_date - curr_day).days
    wait_time = (curr_day - r_info.day_entered).days
    return time_weight(days_til_death) * waiting_weight(wait_time)

def waiting_weight(time):
    return 1.2 ** (time/MAX_TIME)

def time_weight(time):
    if time == "unknown":
        time = MAX_TIME
    return 1.2 ** (time / MAX_TIME)

# want to see how many pairs actually match = about 10%
def check_random_pairing(boost: bool):
    d = Donor()
    r = Recipient()
    w = calculate_weight(d,r)
    
    return w, r.recipient_info

def simulate_matching(number_of_tries: int, boost: bool = False):
    start = timer()

    results = [0,0,0,0,0]
    valid_weights = []
    boosts = []
    for i in range(number_of_tries):
        x, info = check_random_pairing(boost)
        if x < 1:
            if boost:
                m_boost = calculate_boost(info, dt.date.today())
                boosts.append(m_boost)
                x = x*m_boost
            valid_weights.append(x)
        else: 
            results[round(x/1000)] += 1

    print(f"The number of valid matches: {len(valid_weights)}")
    print(f"Average weight: {mean(valid_weights)}")
    if boost:
        print(f"Average boost applied: {mean(boosts)}")
    print(f"Blood incompatibility: {results[1]}")
    print(f"location incompatibility: {results[2]}")
    print(f"Too low: {results[4]}")
    end = timer()
    print(f"Time taken: {end - start}")

if __name__ == '__main__':
    x = input("How many matches to simulate?")

    print('Not including time boost on weightings:')
    simulate_matching(int(x), boost=False)

    print('Including time boost on weightings:')
    simulate_matching(int(x), boost=True)

    print('Done')