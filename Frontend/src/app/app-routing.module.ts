import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DevelopmentComponent } from './development/development.component';
import { LoginComponent } from './login/login.component';
import { OrganExchangeComponent } from './organ-exchange/organ-exchange.component';

const routes: Routes = [
  {path: 'organexchange', component: OrganExchangeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'development', component: DevelopmentComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: 'organexchange'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
