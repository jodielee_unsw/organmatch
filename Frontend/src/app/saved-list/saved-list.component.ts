import { Component, Input, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-saved-list',
  templateUrl: './saved-list.component.html',
  styleUrls: ['./saved-list.component.css']
})

export class SavedListComponent implements OnInit {
  @Input() Pairs!: Pair[];
  constructor() {}

  ngOnInit(): void {
  }
}