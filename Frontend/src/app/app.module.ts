import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { OrganExchangeComponent } from './organ-exchange/organ-exchange.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DevelopmentComponent } from './development/development.component';
import { LoginComponent } from './login/login.component';
import { PairAddComponent } from './pair-add/pair-add.component';
import { LoadGraphComponent } from './load-graph/load-graph.component';
import { SaveGraphComponent } from './save-graph/save-graph.component';
import { NotificationSnackBarComponent} from 'src/app/notification/notification.component';
import { PersonComponent } from './person/person.component';
import { SavedListComponent } from './saved-list/saved-list.component';
import { PairAddWlComponent } from './pair-add-wl/pair-add-wl.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    NavBarComponent,
    OrganExchangeComponent,
    AboutComponent,
    ContactComponent,
    DevelopmentComponent,
    LoginComponent,
    PairAddComponent,
    LoadGraphComponent,
    SaveGraphComponent,
    NotificationSnackBarComponent,
    PersonComponent,
    SavedListComponent,
    PairAddWlComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
