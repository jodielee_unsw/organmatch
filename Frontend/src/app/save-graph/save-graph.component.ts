import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { NotificationService } from '../notification/notification.service';

@Component({
  selector: 'app-save-graph',
  templateUrl: './save-graph.component.html',
  styleUrls: ['./save-graph.component.css']
})
export class SaveGraphComponent implements OnInit {
  @Output() saveRequest = new EventEmitter()
  @Input() name!: string;
  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  SaveGraph() {
    if (this.name == undefined) {
      this.notificationService.showSnackBar('fail', 'Please enter a graph name', false);
      return;
    }
    this.saveRequest.emit()
  }
}
