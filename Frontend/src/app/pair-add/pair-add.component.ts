import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { NotificationService } from '../notification/notification.service';

@Component({
  selector: 'app-pair-add',
  templateUrl: './pair-add.component.html',
  styleUrls: ['./pair-add.component.css']
})
export class PairAddComponent implements OnInit {
  @Output() addRequest: EventEmitter<any> = new EventEmitter();
  @Input() name!: string;

  constructor(private notificationService: NotificationService) { }

  addPair() {
    console.log(this.name);
    if (this.name == undefined) {
      this.notificationService.showSnackBar('fail', 'Please enter a graph name', false);
      return;
    }
    this.addRequest.emit();
  }

  ngOnInit(): void {
  }

}
