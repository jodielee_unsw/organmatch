import { Component } from '@angular/core';

import { NotificationService } from 'src/app/notification/notification.service';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationSnackBarComponent {
    public isSnackBarDismissible = false;
    public isSnackBarVisible = false;
    public snackBarContent: string | undefined;
    public snackBarStatus: Notification.Status | undefined;
    private timeoutId: NodeJS.Timer | undefined;

    constructor(private readonly notificationService: NotificationService) { }

    public ngOnInit(): void {
        this.notificationService.notificationData.subscribe((notificationData: Notification.Data) => {
            this.showSnackBar(notificationData);
        });
    }

    public hideSnackBar(): void {
        this.isSnackBarVisible = false;
    }

    private showSnackBar(notificationData: Notification.Data): void {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
        }

        const { content, status, isDismissible } = notificationData;
        this.snackBarContent = content;
        this.snackBarStatus = status;
        this.isSnackBarDismissible = isDismissible;
        this.isSnackBarVisible = true;
        if (!this.isSnackBarDismissible) {
            this.autoHideSnackBar();
        }
    }

    private autoHideSnackBar(): void {
        this.timeoutId = setTimeout(() => { this.hideSnackBar(); }, 3000);
    }
}
