import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    public notificationData = new EventEmitter<Notification.Data>();

    public showSnackBar(status: Notification.Status, content: string, isDismissible: boolean): void {
        this.notificationData.emit({ status, content, isDismissible });
    }
}
