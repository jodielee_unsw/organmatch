import { NONE_TYPE } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import { GraphService } from '../graph/graph.service';
import { WaitlistService } from '../graph/waitlist.service';

@Component({
  selector: 'app-organ-exchange',
  templateUrl: './organ-exchange.component.html',
  styleUrls: ['./organ-exchange.component.css']
})
export class OrganExchangeComponent implements OnInit {
  graph: any;
  size: any;
  pairs: any;
  name: any;
  saved: Pair[] = [];
  wl: any;
  wls: any;

  constructor(private graphService: GraphService, private waitlistService: WaitlistService) {}

  ngOnInit(): void {
  }
  saveGraph() {
    console.log(this.name)
    this.graphService.saveGraph().subscribe(g=> {});
  }

  loadGraph(load: string) {
    this.graphService.selectGraph(load).subscribe(g => {
      this.graph = g.Graph;
      this.size = g.Size;
      this.name = g.Name;
      this.pairs = g.Pairs;
    })
    this.waitlistService.selectWL(load).subscribe(wl => {
      this.wl = wl.WaitList;
      this.wls = wl.Size;
    })
  }

  addPairRandom() {
    if (this.name == undefined) {
      console.log("Please Enter a Graph Name")
      return
    }
    this.graphService.addToGraph({Random: true}).subscribe(g => {
      this.saved = g.Pairs;
      console.log(this.saved);
    });
    setTimeout(() => {}, 300);
    this.graphService.getGraph().subscribe(g => {
      this.graph = g.Graph;
      this.size = g.Size;
      this.name = g.Name;
      this.pairs = g.Pairs;
    })
  }
  addPairWL() {
    this.waitlistService.addToWL({Random: true, Recipient: Object})
    //this.waitlistService.getWL().subscribe(wl => {
    //  this.wl = wl.WaitList
    //})
  }
}
