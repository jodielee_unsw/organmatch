import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganExchangeComponent } from './organ-exchange.component';

describe('OrganExchangeComponent', () => {
  let component: OrganExchangeComponent;
  let fixture: ComponentFixture<OrganExchangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganExchangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
