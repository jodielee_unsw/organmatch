import { Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-load-graph',
  templateUrl: './load-graph.component.html',
  styleUrls: ['./load-graph.component.css']
})
export class LoadGraphComponent implements OnInit {
  @Output() loadRequest = new EventEmitter<string>();

  constructor() { }

  onKey(event: any) {
    const inputValue = event.target.value;
    this.loadRequest.emit(inputValue);
  }

  ngOnInit(): void {
  }

}
