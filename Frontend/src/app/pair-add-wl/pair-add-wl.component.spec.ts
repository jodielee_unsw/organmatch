import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PairAddWlComponent } from './pair-add-wl.component';

describe('PairAddWlComponent', () => {
  let component: PairAddWlComponent;
  let fixture: ComponentFixture<PairAddWlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PairAddWlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PairAddWlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
