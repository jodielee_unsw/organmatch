import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-pair-add-wl',
  templateUrl: './pair-add-wl.component.html',
  styleUrls: ['./pair-add-wl.component.css']
})
export class PairAddWlComponent implements OnInit {
  @Output() addRequestR: EventEmitter<any> = new EventEmitter();
  @Input() name!: string;

  constructor() { }

  ngOnInit(): void {
  }

  addRecipient() {
    this.addRequestR.emit();
  }
}
