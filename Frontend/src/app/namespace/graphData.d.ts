interface Pair {
    Donor: Donor,
    Recipient: Recipient,
  }
  
interface Donor {
  name: string,
  bloodType: string,
  age: number,
  timeTilDeath: number
}

interface Recipient {
  name: string,
  bloodType: string,
  age: number
}

interface Graph {
  Graph: number[][];
  Pairs: Pair[];
  Size: number;
  Name: string;
}

interface Saved {
  Pairs: Pair[];
  Transplants: number;
}