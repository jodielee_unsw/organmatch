declare namespace Notification {
    interface Data {
        status: Status;
        content: string;
        isDismissible: boolean;
    }

    type Status = 'success' | 'fail' | 'warning' | 'info';
}