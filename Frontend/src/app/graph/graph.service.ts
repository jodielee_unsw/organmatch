import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class GraphService {
  uri = 'http://127.0.0.1:5000/'
  constructor(private http: HttpClient) { }

  selectGraph(name: string) {
    return this.http.get<Graph>(`${this.uri}/graph/init?name=${name}`)
  }

  addToGraph(data: any) {
    return this.http.post<Saved>(`${this.uri}/graph/add`, data)
  }

  getGraph() {
    return this.http.get<Graph>(`${this.uri}/graph/show`)
  }

  saveGraph() {
    return this.http.post(`${this.uri}/graph/save`, {})
  }
}
