import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
interface WaitList {
  WaitList: object[];
  Name: string;
  Size: number;
}
interface AddData {
  Random: boolean;
  Recipient: object;
}
@Injectable({
  providedIn: 'root'
})
export class WaitlistService {
  uri = 'http://127.0.0.1:5000/'
  constructor(private http: HttpClient) { }

  selectWL(name: string) {
    return this.http.get<WaitList>(`${this.uri}/waitlist/init?name=${name}`)
  }

  addToWL(data: AddData) {
    return this.http.post(`${this.uri}/waitlist/add`, data)
  }

  getWL(name: string) {
    return this.http.get<WaitList>(`${this.uri}/waitlist/show`)
  }

  saveWL() {
    return this.http.post(`${this.uri}/waitlist/save`, {})
  }
}
